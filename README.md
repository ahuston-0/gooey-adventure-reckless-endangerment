# Players
## Alice
* 2 Power Up Points
* Blood-Touched Agent of Jinxx
  * AC = 13
  * Attacks
    * Vicious Dagger
    * +1 to hit, 1d4 DMG
  * Abilities
    * Find and Disable Traps
      * +3 to trap rolls
    * Scout
      * Goes ahead of group, gives advantage for first roll of combat
    * Lock Pick (from Gnome Rogue)
      * +3 to picking locks
  * Power Up Abilties
    * Blinding Trick
      * Skip past one combat encounter, only if there is one opponent
    * Fool's Luck
      * The party will find a favorable discovery in the next room
    * Party Evasion
      * Allows the party to avoid the effects of a trap
  * Items
    * White Crystal (symbol of Evoqe)
      * Heals for +1 HP
      * Used
    * Scroll of Mind Restoration
      * Restores any two spells, arcane or divine

## Tyler
* 3 Power Up Points
* Sofling Wizard
  * AC = 12
  * Attacks (Do not cost spell slots)
    * Trident
      * +2 to hit, 1d8+2 DMG
    * Searing Ray
      * +1 to hit, 1-3 DMG
    * Burning Fury
      * Can be used twice per rest
      * +2 to hit, 3 DMG
  * Abilities
    * Currently has 4 of 5 spell slots
    * Spellcasting
      * 5 spell slots, 1st level
      * Disguise Self
      * Feather Fall
      * Identify
      * Magic Missile
      * Silent Image
      * Comprehend Languages
      * Mage Armor
    * Investigation
      * +3 to investigation checks
  * Power Up Abilities
    * What a Lucky Party
      * Find a secret within the current room. The secret is up to the GM's discretion.
    * Zyathéan Magic
      * The wizard can take (deal?) max damage on any one spell attack.
    * Magic Reserve
      * Transfer one power up point to another player
  * Items
    * Old rusted metal spike
    * Trident

## Chase
* 2 Power Up Points
* Human Paladin of Radean
  * AC = 18
  * Attacks
    * Divine Sword
      * +2 to hit, 1d8 DMG
    * Broadsword
      * +2 to hit, 2d6+2 DMG
  * Abilities
    * Currently has 1 of 2 spell slots
    * Currently has 1 use of Lay on Hands left
    * Lay on Hands
      * Can be used once per rest
      * Maximum of 1 HP party healing
    * Spellcasting
      * 2 spells slots, 1st level
      * Shield of Faith
      * Detect Magic
      * Command
      * Detect Evil and Good
  * Power Up Abilities
    * Divine Sense
      * The party can avoid one combat encounter
    * Deific Knowledge
      * The GM gives the paladin insight and lore regarding the current room
    * Sunlight Smite
      * Find any secret paths in a particular room
  * Items
    * Platinum coin of ancient mintage
      * Treasure redeemable at the end of the adventure
    * Ring of Healing (2 spells of 4 HP healing)
    * Broadsword

## Alex
* 3 Power Up Points
* Goruund Barbarian
  * AC = 15
  * Attacks
    * Raging Axe
      * +3 to hit, 1d8 DMG
  * Abilities
    * Rage
      * Receive advantage on initiative rolls and attack rolls for one round
    * Strength
      * Advantage on all strength checks
    * Find and Disable Traps (from Gnome Rogue)
      * +3 to trap rolls
  * Power Up Abilities
    * Danger at Hand
      * Warn party of impending danger
      * +1 Temporary HP
    * Shared Relentless Endurance
      * When the party is at 0 HP, drop to 1 HP instead
    * Extra Attack
      * Can only be used once per turn
      * Attack again
  * Items
    * Box with 100 gold coins

## Kelsey
* 1 Power Up Points
* Human Cleric of Buffahn
  * AC = 14
  * Attacks
    * Menacing Mace
      * +1 to hit, 1d6 DMG
    * Short sword
      * +2 to hit, 1d6+2 DMG
  * Abilities
    * Healing
      * Heals the party for 1 HP after every combat encounter
    * Spell Casting
      * Currently has 5 of 5 spell slots
      * 5 spell slots, 1st level
      * Bless
      * Bane
      * Detect Evil & Good
      * Detect Magic
      * Healing Word (Heal 1 HP)
      * Cure Wounds (Heal 1 HP)
  * Power Up Abilities
      * Servant of the Brew Master
        * Once per point, the party regains 2 HP instead of 1 HP
      * Channel Buffahn
        * Must be used outside of combat
        * Regain all first level spell slots
      * Ale Between Friends
        * Once per point, the party gains 3 HP
  * Items
    * Short sword
    * Haela Shroom
      * May heal for 1d4 HP and may cause nausea

## Party Specifics
* Party AC = 10
* HP = 3

# Choice History
* Go to Standing Stones (10 HP)
* Standing Stones Actions (10 HP)
	* Alice inspects traps
	* Kelsey checks for magic
	* Chase checks the ground inside the circle
	* Tyler inspects the area
	* Tyler inspects a single stone
	* Alex prepares for battle
  * Tyler and Alice take old rusted metal spike and white crystal respectively
  * Party enters dungeon
  * Inspect the skull headed statue, then progress through either the north or the south portal
  * The statue is flanked by two large braziers. The statue itself is about 10 feet tall and entirely made of stone. It holds a large shield with a glowing circular device inscribed upon it.
* Statue Actions (10 HP)
  * Alex searches around the base of the statue and the alcove and uses Find and Disable Traps
  * Alice inspects the statue itself and uses Find and Disable Traps
  * Chase inspects the south brazier
  * Kelsey inspects the strange glowing device on the shield to try to determine what it is used for or might be used for, and uses Detect Magic on the shield
  * Tyler is inspecting the the north brazier and examining the statue itself for any secret compartments
  * Alex finds a loose stone with what may be a switch of some type
  * Alice finds a trap that can likely not be disabled
  * Chase finds a flat, circular device in the south brazier
  * There are nothing but old coals visible in the north brazier
  * The device radiates evocation, abjuration, conjuration, and necromantic magicks There is a indentation of a left hand on the device
* Bonus Statue Results (10 HP)
  * Alex finds a broadsword after activating the switch
  * Chase finds a platinum coin of ancient mintage;this may be redeemed at the end of the adventure
  * There is nothing of interest in the north brazier
  * Kelsey is shocked and loses a single hit point
    * The party hears "Where the dark flame burns... therein a danger lurks. Seek my seal that you may pass. Choose wrongly and we shall meet. Trust not in that which you may trust."
  * Tyler casts identify on the sword and the party goes south
* South entrance (9 HP)
  * The broadsword +2 to hit, 2d6+2 DMG. The party needs to give it to someone
  * The south entrance is barred by a lock, there are the skeletal remains of a human or elf on the floor
  * Alex inspects the gate using Find and Disable Traps
  * Alice attempts to pick the lock using +3 to lockpicking
  * Kelsey inspects the skeleton lying on the floor
  * Chase searches the west wall for secret passages
  * Tyler searches the east wall for secret passages using +3 to investigation
  * Tyler inspects the ceiling above the party for potential dangers using +3 to investigation
  * Alex disarms the traps
  * Alice picks the lock and hears a slithering noise
  * Kelsey finds a ring
  * Tyler finds a small opening in the east wall
  * The party sees a large slithering creature, with an AC of 12
* Worm!!! (9 HP)
  * Round 1 (9 HP)
    * Chase takes the ring, attacks with his Divine Sword (miss)
    * Kelsey attacks with her mace (miss)
    * Alice uses her vicious dagger (3 DMG)
    * Tyler attempts to open the covering in the stone and uses Searing Ray (miss)
    * Tyler finds a short sword behind the covering
    * The worm hits the party for 2 damage
    * Total DMG
      * Party takes 2 DMG
      * Worm takes 3 DMG
  * Round 2 (7 HP)
    * Chase casts detect magic on the short sword and the ring
    * The ring has two spells of healing for 4 HP, may be used by a cleric or paladin only
    * The sword does 1d6+2 DMG to target, +2 to hit
    * Alice attacks with her vicious dagger (miss)
    * Alice uses white crystal (+1 HP)
    * Chase gives short sword to Kelsey
    * Chase takes Alex's broadsword
    * Chase attacks with the broadsword (6 DMG)
    * Kelsey uses short sword (7 DMG)
    * Tyler uses Searing Ray (miss)
    * Chase attacks again with broadsword (miss)
    * Alex uses Raging Axe (miss)
    * Total DMG
      * Party takes 3 DMG and heals for 1 HP
      * Worm takes 13 DMG and dies
* The Cavern (5 HP)
  * The cavern is incredibly musty, about 10ft tall but slopes down. Covered in glowing mushrooms and dripping with water. There's an alcove to the east and something's in the shadows. There's a faint glimmer of metal in the webbing to the south. To the west, the passage continues.
  * Kelsey casts Ale Between Friends (+3 HP)
  * Kelsey examines the glowing mushrooms
  * Alice inspects the shiny rock on the west wall w/ find and disable traps
  * Alex searches the area with the webbing w/ find and disable traps
  * Chase concentrates on Detect magic
  * Tyler searches the east alcove
  * Alice finds a switch in the west wall, as a bonus action it can be lowered
  * Tyler finds a dry, dead body with a trident that can be taken as a bonus action
  * Alex realizes that the glimmer of metal is actually a trap door, which can be opened as a bonus action
  * Kelsey finds a Haela Shroom that may heal the party for 1d4 HP but also may cause nausea, and it can be taken as a bonus action
  * Chase feels magic coming from the strange egg shaped things, but it may be interference from the cavern
* Spiders (8 HP)
  * Round 1 (8 HP)
    * Two enormous spiders come rushing out (AC 12), the party may roll to hit as a bonus action
    * Tyler uses Burning Fury (3 DMG)
    * Chase uses his Divine Sword (2 DMG)
    * Alex uses Raging Axe (3 DMG)
    * Alice uses vicious dagger (1 DMG)
    * Alice finds a dead body and a scroll inside the panel that was triggered by the switch
    * Tyler takes the trident (+2 to hit, 1d8+2 DMG)
    * Alex moves a handle and opens a nearby trap door, and may investigate it as a bonus action
    * Kelsey takes the Haela Shroom for later
    * One of the spiders is damaged and they bite the party, dealing 2 DMG
    * The party must make a 1d20+3 roll (unspecified reason)
    * Total DMG
      * Party takes 2 DMG
      * Spiders take 9 DMG
  * Round 2 (6 HP)
    * Chase makes a save for 5
    * Alice takes the scroll and looks inside
    * Alex investigates behind the trap door
    * Alex would like to rage and using Raging Axe (3 DMG)
    * Chase attacks with Divine Sword (miss)
    * Kelsey attacks with the short sword (8 DMG)
    * Tyler uses Searing Ray (2 DMG)
    * Alice attacks with vicious dagger (miss)
    * Alice finds out the scroll is one of Mind Restoration (restores two spells already casted, arcane or divine)
    * Alex finds 100 Golds in a box inside the trap door
    * The party must make 2 consecutive 1d20+3 rolls
    * Total DMG
      * Party takes 3 DMG
      * Spiders take 13 DMG
  * Round 3 (3 HP)
    * Alice makes a save for 19 and 16
    * Tyler spends 1 Power Point for Zyathéan Magic and casts magic Missile (15 DMG)
    * Alice uses vicious dagger (miss)
